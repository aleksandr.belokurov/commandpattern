package sber.belokurov.commmand.pattern;

import sber.belokurov.commmand.pattern.controller.TestController;

public class Main {
    public static void main(String[] args) {
        TestController testController = new TestController();
        testController.startTestApplication();
    }
}
