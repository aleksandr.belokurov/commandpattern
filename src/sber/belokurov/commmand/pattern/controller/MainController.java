package sber.belokurov.commmand.pattern.controller;

import sber.belokurov.commmand.pattern.controller.command.EmailNotification;
import sber.belokurov.commmand.pattern.controller.command.Notification;
import sber.belokurov.commmand.pattern.controller.command.SmsNotification;
import sber.belokurov.commmand.pattern.database.Base;
import sber.belokurov.commmand.pattern.datalayer.Client;
import sber.belokurov.commmand.pattern.view.BasicGUI;

import java.util.ArrayList;
import java.util.List;

public class MainController {

    private static final BasicGUI BASIC_GUI = new BasicGUI();
    private static final Base BASE = Base.getInstance();

    private List<Notification> notificationList = new ArrayList<>();

    public MainController() {

    }

    public Client chooseClient() {
        String name = BASIC_GUI.printMenuChooseClient();
        ;
        Client client = BASE.getClient(name);
        while (client == null) {
            BASIC_GUI.printClientNotFound();
            name = BASIC_GUI.printMenuChooseClient();
            client = BASE.getClient(name);
        }
        return client;
    }

    public void chooseNotification() {
        int number = BASIC_GUI.printMenuChooseCommand();
        switch (number) {
            case (1):
                notificationList.add(new SmsNotification());
                break;
            case (2):
                notificationList.add(new EmailNotification());
                break;
            case (3):
                notificationList.add(new SmsNotification());
                notificationList.add(new EmailNotification());
                break;
            default:
                BASIC_GUI.printError();
        }
    }

    public void executeNotification(Client client) {
        notificationList.forEach((e) -> e.sendNotify(client));
    }

}
