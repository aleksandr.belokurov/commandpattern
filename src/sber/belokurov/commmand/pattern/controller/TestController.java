package sber.belokurov.commmand.pattern.controller;

import sber.belokurov.commmand.pattern.database.Base;
import sber.belokurov.commmand.pattern.datalayer.Client;
import sber.belokurov.commmand.pattern.view.BasicGUI;

public class TestController {
    private static final BasicGUI BASIC_GUI = new BasicGUI();
    private static final Base BASE = Base.getInstance();
    private static final MainController MAIN_CONTROLLER = new MainController();

    public TestController() {

    }

    public void startTestApplication() {
        fillInData();
        BASIC_GUI.printClients(BASE.getClients());
        Client client = MAIN_CONTROLLER.chooseClient();
        MAIN_CONTROLLER.chooseNotification();
        MAIN_CONTROLLER.executeNotification(client);
    }

    public void fillInData() {
        BASE.addClient(new Client("Alex", "332211", "Alex@sb.ru"));
        BASE.addClient(new Client("Aleksandr", "554433", "Aleksandr@sb.ru"));
        BASE.addClient(new Client("Marina", "112244", "Marina@sb.ru"));
    }
}
