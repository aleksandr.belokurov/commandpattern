package sber.belokurov.commmand.pattern.controller.command;

import sber.belokurov.commmand.pattern.datalayer.Client;

public interface Notification {
    void sendNotify(Client client);
}
