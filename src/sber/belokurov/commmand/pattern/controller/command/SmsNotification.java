package sber.belokurov.commmand.pattern.controller.command;

import sber.belokurov.commmand.pattern.datalayer.Client;

public class SmsNotification implements Notification{
    @Override
    public void sendNotify(Client client) {
        //Реализация отправки
        System.out.println("Сообщение отправлено " + client.getName() + " на телефон " + client.getPhone());
    }
}
