package sber.belokurov.commmand.pattern.database;

import sber.belokurov.commmand.pattern.datalayer.Client;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Base {

    private static Base instance = null;

    private Base() {

    }

    public static Base getInstance() {
        if (instance == null) {
            instance = new Base();
        }
        return instance;
    }

    List<Client> clients = new ArrayList<>();

    public Client getClient(String name) {
        Optional<Client> client = clients.stream().filter((e) -> e.getName().equals(name)).findFirst();
        return client.orElse(null);
    }

    public List<Client> getClients() {
        return clients;
    }

    public void addClient(Client client) {
        clients.add(client);
    }
}