package sber.belokurov.commmand.pattern.view;

import sber.belokurov.commmand.pattern.datalayer.Client;

import java.util.List;
import java.util.Scanner;

public class BasicGUI {

    public BasicGUI() {

    }

    public String printMenuChooseClient() {
        System.out.println("Введите имя пользователя которому хотите отправить сообщение");
        Scanner in = new Scanner(System.in);
        return in.next();
    }

    public int printMenuChooseCommand() {
        System.out.println("Выберите как вы хотите отправить ему сообщение \n " +
                "1 sms \n " +
                "2 email \n " +
                "3 sms и email");
        Scanner in = new Scanner(System.in);
        return in.nextInt();
    }

    public void printClients(List<Client> clients) {
        clients.forEach(System.out::println);
    }

    public void printClientNotFound() {
        System.out.println("Клиент с таким именем не найден");
    }

    public void printError() {
        System.out.println("Не удалось отправить сообщение попробуйте позже");
    }
}
